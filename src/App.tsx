import { useEffect, useState } from "react";
import { useQuery } from "react-query";

// Import Components
import { Drawer, LinearProgress, Grid, Badge } from "@material-ui/core";
import { AddShoppingCart } from "@material-ui/icons";

// Import Styles
import { Wrapper } from "./App.styles";

export type CartItemType = {
  id: number;
  category: string;
  description: string;
  image: string;
  price: number;
  title: string;
  amount: number;
}

const getProducts = async (): Promise<CartItemType[]> => await (await fetch('https://fakestoreapi.com/products')).json()


const App = () => {

  const {data, isLoading, error} = useQuery<CartItemType[]>(
    'products',
    getProducts
  );

  console.log(data)
  
  return (
    <div className="App">
      Start
    </div>
  );
}

export default App;
